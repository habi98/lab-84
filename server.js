const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

const users = require('./app/users');
const tasks = require('./app/tasks');



const app = express();


app.use(express.json());
app.use(cors());

const port = 8000;


mongoose.connect(config.dbUrl, config.mongooseOptions).then(() => {

    app.use('/users', users);
    app.use('/tasks', tasks);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    })
});