const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/', auth,  (req, res) => {
    const taskData = req.body;
    const task = new Task(taskData);

    task.user = req.user._id;

    task.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(400).send(error))
});


router.get('/', auth, (req, res) => {

    Task.find({user: req.user._id})
        .then(tasks => res.send(tasks))
        .catch(() => res.sendStatus(500));
});

router.put('/:id',  (req, res) => {
    Task.updateOne({_id: req.params.id}, {$set : req.body})

        .then(task => res.send(task))
        .catch(error => res.status(500).send(error))

});



router.delete('/:id', async (req, res) => {
    const task = await Task.findByIdAndDelete({_id: req.params.id});

    if (!task) {
        return res.status(401).send({error: 'Task not found'})
    }
    
     res.send({message: 'task successfully deleted'})
});


module.exports = router;