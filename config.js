const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    dbUrl: 'mongodb://localhost/todo-list',
    mongooseOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
    }
}